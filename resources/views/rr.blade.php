<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('fonts/icomoon/style.css')}}">

    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <!-- Style -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <title>Login #7</title>
</head>
<body>



<div class="content">
    <div class="container">
        <div class="row">
            <div>
                <div>{{$userinf}}
                </div>
                <div>{{$tasks}}</div>
            </div>

        </div>
    </div>
</div>



<form action="{{route('CreateTask')}}" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        <div class="col-md-12 ">
            <div class="form-group first mt-4" style="padding: 6px">
                <label for="user_id">User Name</label>
                <select  class="form-select form-group " style="width: 100%" aria-label="Default select example" style="padding: 6px" name="user_id" id="user_id">
                    @foreach($userinf as $userin)
                        <option value="{{$userin->id}}" >{{$userin->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group first mt-4" style="padding: 6px">
                <label for="name">Task Name</label>
                <input type="text" name="name" class="form-control form-group ">

            </div>
            <div class="form-group first mt-4" style="padding: 6px">
                <label for="description">Description</label>
                <input type="text" name="description" class="form-control form-group ">

            </div>
            <label for="user_id">Status</label>
            <select class="form-select form-group "style="width: 100%"  aria-label="Default select example" style="padding:6px  " name="status" id="status">

                <option value="pending">pending</option>
                <option value="inprogress">inprogress</option>
                <option value="completed">completed</option>
            </select>
        </div>

    </div>


    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <input type="submit" value="Submit" class="btn btn-primary">

</form>


<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
</body>
</html>
