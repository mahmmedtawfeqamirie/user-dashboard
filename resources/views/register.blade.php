<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('fonts/icomoon/style.css')}}">

    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <!-- Style -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <title>Login #7</title>
</head>
<body>
<div class="content" style="padding: 3rem">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="images/undraw_remotely_2j6y.svg" alt="Image" class="img-fluid">
            </div>
            <div class="col-md-6 contents">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="mb-4">
                            <h3>Sign In</h3>
                            <p class="mb-4">welcome back with us.</p>
                        </div>
                        <form action="{{route('register')}}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group first mt-4" style="padding: 6px">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control">

                            </div>
                            <div class="form-group first mt-4" style="padding: 6px">
                                <label for="email">Email</label>
                                <input type="email" name="email" class="form-control">

                            </div>

                                    <label for="birthday">Birthday</label>
                                    <input class="form-group last mb-4 mt-4 input--style-4 js-datepicker" type="date" name="birthday">
                                    <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>

                            <div class="form-group  mb-4 mt-4" style="padding: 6px">
                                <label for="password">Password</label>
                                <input type="password" name="password" class="form-control" id="password">

                            </div>
                            <div class="form-group last  mb-4 mt-4" style="padding: 6px">
                                <label for="confirm_password">confirm_password</label>
                                <input type="password" name="confirm_password" class="form-control">

                            </div>

                            <select name="gender" id="">
                                <option value="male">male</option>
                                <option value="female">female</option>
                            </select>


                            <input type="submit" value="Register" class="btn btn-block btn-primary">


                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>


<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
</body>
</html>
