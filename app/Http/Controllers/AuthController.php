<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditInfoUserRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function register(RegisterUser $request)
    {
        $data = $request->validated();
        unset($data['confirm_password']);
        $created = $this->user->create($data);
        return redirect()->route('home');
    }

    public function login(LoginRequest $request)
    {
        $data = $request->validated();
        if (Auth::attempt($data))
            return redirect()->route('home');
        else
            return redirect()->route('login');
    }

    public function logOut(Request $request)
    {
        Auth::logout();
        return redirect()->route('login');
    }

    public function edit(EditInfoUserRequest $request, $usertId)
    {
        $data = $request->validated();

        unset($data['confirm_password']);
        if ($data['password']== null)
            unset($data['password']);
        else
            $data['password']= Hash::make(($data['password']));
        if ($this->user->find(['id', $usertId])->first() ) {
            $user = $this->user->where('id', $usertId)->update($data);
            return redirect()->route('getUser');
        }
       }

    public function delete( $userId)
    {
        $userId = $this->user->find(['id' => $userId])->first();
        if ($userId) {
            $userId->delete();
            return redirect()->route('getUser');
        }

    }
    public function getUser( )
    {
      $userinf=  $this->user->all();
        $getUsertrue =1;
        return view('home',compact(['userinf','getUsertrue']));
    }
}
