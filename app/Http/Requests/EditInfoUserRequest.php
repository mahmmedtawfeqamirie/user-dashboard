<?php

namespace App\Http\Requests;

use App\Enums\GenderEnum;
use App\Models\User;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class EditInfoUserRequest extends FormRequest
{

    public function authorize()
    {
        $userId = $this->route('userId');
        return(User::where('id', $userId)->exists()||Auth::user()->is_Admin==1);
    }

    public function rules()
    {
        return [
            'name' => [ 'string'],
            'email' => [ 'email'],
            'password' => [ 'nullable','string'],
            'confirm_password' => [ 'nullable','string', 'same:password'],
            'birthday' => [ 'date'],
            'gender' => [ 'string',new EnumValue(GenderEnum::class),],
  ];
    }
}
