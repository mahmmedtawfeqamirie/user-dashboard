<?php

namespace App\Http\Requests;

use App\Enums\GenderEnum;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterUser extends FormRequest
{

    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'email' => ['required', 'email', Rule::unique('users', 'email')],
            'password' => ['required', 'string'],
            'confirm_password' => ['required', 'string', 'same:password'],
            'birthday' => ['nullable', 'date'],
            'gender' => ['required', 'string',new EnumValue(GenderEnum::class),],
        ];
    }
}
